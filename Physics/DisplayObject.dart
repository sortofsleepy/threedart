part of THREE;


class DisplayObject{
    var acceleration = Three.Vector3();
    var position = Three.Vector3();
    var velocity = Three.Vector3();



    //physics props
    double mass = 10.0;

    //constructor
    DisplayObject(){
        velocity["x"] = 1;
        velocity["y"] = 3.3;
    }

    /**
     * Adds a force to the object
     */
    void addForce(var vec){
        var copy = Three.Vector3();
        copy = vec;

        copy.divideScalar(mass);

        acceleration.add(copy);
    }

    /**
     * Update all positions
     */
    void update(){
        velocity.add(acceleration);
        position.add(velocity);
    }

    /**
     * Check for collisions against the world,
     * assumed to be the browser window.
     */
    void checkWalls(){
        if((position["x"] > window.innerWidth) || (position["x"] < 0)){
            velocity["x"] *= -1;
        }

        if((position["y"] > window.innerHeight) || (position["y"] < 0)){
            velocity["y"] *= -1;
        }
    }

}
