part of THREE;

class FlowField {

    var field = [[]];

    double cols,rows;

    int resolution = 10.0;

    Random random;
    FlowField(){

        cols = window.innerWidth / resolution;
        rows = window.innerHeight / resolution;

        random = new Random();
        double xoff =0;
        for(var i = 0;i<cols.floor();++i){
            fouble yoff = 0;
            for(var j = 0;j<rows.floor();++j){
                double x = i * resolution;
                double y = j * resolution;

                var vec = new Vector3(x,y,0.0);
                double theta = Maths.map(random.nextDouble() * yoff,0.0,1.0,0.0,(PI * PI));

                vec["x"] = cos(theta);
                vec["y"] = sin(theta);
                field.insert(i*j,vec);

                yoff += 0.1;


            }
            xoff += 0.1;
        }

    }

    JsObject getLocation(JsObject location){
        int column = Maths.constrain(location["x"]/resolution,0,cols-1);
        int row = Maths.constrain(location["y"]/resolution,0,rows-1);
        return field[column * row];
    }

}
