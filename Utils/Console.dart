part of THREE;

class Console{

    static String NOTICE = "NOTICE ";
    static String WARNING = "WARNING ";
    static String ERROR = "ERROR ";

    Console(){}
    static log(method,{String level: "notice"}){
        context["console"].callMethod("log",[level,method]);
    }
}
