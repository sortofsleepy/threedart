import 'package:vector_math/vector_math.dart';

class Particle{
    Vector3 position;
    Vector3 acceleration;
    Vector3 velocity;

    double mass = 10.0;

    Particle(){
        position = new Vector3(0.0,0.0,0.0);
        acceleration = new Vector3(0.0,0.0,0.0);
        velocity = new Vector3(0.0,0.0,0.0);

    }

    void addForce(Vector3 vec){
        Vector3 copy = vec.clone();
        copy /= mass;

        acceleration += copy;
    }

    void update(){
        velocity += acceleration;
        position += velocity;
    }

}
