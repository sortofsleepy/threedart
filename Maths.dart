part of THREE;

class Maths{
    static int constrain(int amt, int low, int high) {
        return (amt < low) ? low : ((amt > high) ? high : amt);
    }

    /**
     * Maps a value to a range. Taken from Processing
     */
    static double map(double val, double istart, double istop, double ostart, double ostop){
        return ostart + (ostop - ostart) * ((val - istart) / (istop - istart));
    }
}
