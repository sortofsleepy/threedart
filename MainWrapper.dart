part of THREE;

/**
 * A simple wrapper to make using THREE a little bit nicer
 */
class Three{

    Three(){}

    static Stats(){
        var stats = new JsObject(context["Stats"]);
        stats["domElement"].style.position = "absolute";
        return stats;
    }

    static make(String object,[props=null]){
        var obj;
        if(props != null){

            if(props is List){
                obj = new JsObject(context["THREE"][object],props);

            }else {
                obj = new JsObject(context["THREE"][object]);

                loop(k, v) {
                    if (obj.hasProperty(k)) {
                        obj[k] = v;
                    }
                }
                ;

                props.forEach(loop);
            }

            return obj;

        }else{
            return new JsObject(context["THREE"][object]);
        }
    }

    static Vector3([double x,double y,double z]){
        return new JsObject(context["THREE"]["Vector3"],[x,y,z]);
    }

    static WebGLRenderer({params:null}){
        var renderer = new JsObject(context["THREE"]["WebGLRenderer"]);

        if(params != null){

            loop(k,v){
                if(renderer.hasProperty(k)){
                    renderer[k] = v;
                }
            };

            params.forEach(loop);
        }

        return renderer;
    }

    /**
     * Creates a PerspectiveCamera object
     */
    static PerspectiveCamera(double fov,double aspectRatio,double near,double far){
        var camera = new JsObject(context["THREE"]["PerspectiveCamera"],[
            fov,aspectRatio,near,far
        ]);

        return camera;
    }

    /**
     * Makes a new Scene
     */
    static Scene(){
        return  new JsObject(context["THREE"]["Scene"]);
    }


    /**
     * Creates a new Mesh
     */
    static Mesh(JsObject geometry,JsObject material){
        var mesh = new JsObject(context["THREE"]["MeshBasicMaterial"],[geometry,material]);
        return mesh;
    }


    /**
     * Wrapper to create a Mesh Basic material
     */
    static MeshBasicMaterial(params){
        var material = new JsObject(context["THREE"]["MeshBasicMaterial"]);
        loop(k,v){
            if(material.hasProperty(k)){
                material[k] = v;
            }
        };
        params.forEach(loop);
    }


}
