part of THREE;

class Material{
    var attributes = [];
    var uniforms = [];

    var mat;
    var vertex = "";
    var fragment = "";

    Material(){}

    void addAttribute(var attribute){
        attributes.add(attribute);
    }

    void addUniform(var uniform){
        uniforms.add(uniform);
    }

    void setVertexShader(){

    }

    void setFragmentShader(){

    }

    void compose(){
        attributes = new JsObject.jsify(attributes);
        uniforms = new JsObject.jsify(uniforms);
        mat = Three.make("ShaderMaterial",{
            "attributes":attributes,
            "uniforms":uniforms,
            "vertexShader":Loader.loadShader("./dart/THREE/shaders/test_vert.glsl"),
            "fragmentShader":Loader.loadShader("./dart/THREE/shaders/test_fragment.glsl")
        });
    }

    JsObject getObject(){
        return mat;
    }




}
