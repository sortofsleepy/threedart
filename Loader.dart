part of THREE;

/**
 * Basic loading class for a lot of things.
 */
class Loader{

    Loader(){}

    static String loadShader(String path){
        var url = "./" + path;
        var req = new HttpRequest();
        req.open("GET",url,async:false);
        req.send();

        if(req.readyState == 4) {
            if(req.status == 200){
                return req.responseText;
            }
        }
    }

    /**
     * Loads a texture using Three's texture loader
     */
    static JsObject loadTexture(String path){
        var three = new JsObject(context["THREE"]["ImageUtils"]);
        return three.callMethod("loadTexture",[path]);
    }
}
